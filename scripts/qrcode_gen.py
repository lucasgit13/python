#!/usr/bin/env python

"""
Target: Generate a qrcode from text on clipboard
Target OS: GNU/Linux
Interface: CLI
"""

import os
import qrcode
import pyperclip

VERSION = '1.0.0'

text = pyperclip.paste()

IMAGEPATH = "/tmp/qrcode.png"
img = qrcode.make(text)

img.save(IMAGEPATH)
startfile = 'sxiv ' +  IMAGEPATH
res = os.system(startfile)

print("output:", res)
print("version:", VERSION)
