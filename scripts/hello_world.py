#!/usr/bin/env python
#
"""
Target: Get the computer to output a message
Target Users: Me
Target OS: GNU/Linux
Interface: CLI
"""

# Print simple messsage
print("Hello World")

# Get text from user
some_text = input('Type in some text: ')

# Print out the inputed text
print(some_text)



