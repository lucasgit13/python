#!/usr/bin/env python

# import OS module
import os

# Get the list of all files and directories
user = os.getenv("HOME")
pathset = input("~/")
path = user + "/" + pathset
files = os.listdir(path)

for f in files:
    print(f)
