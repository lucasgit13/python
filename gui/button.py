#!/usr/bin/env python

from tkinter import *

root = Tk()

def myClick():
    myLabel = Label(root, text="Click me!").pack()

myButton = Button(root, text="Click me!", command=myClick).pack()

root.mainloop()
