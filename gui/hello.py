#!/usr/bin/env python

from tkinter import *

root = Tk()

myLabel1 = Label(root, text="Hello Python!")
myLabel2 = Label(root, text="This is line 2")

myLabel1.grid(row=0,column=0)
myLabel2.grid(row=1,column=1)

root.mainloop()
